function eulerOne() {
    var number = document.getElementById("problem-one").value;
    var sum = 0;
    for (var i = 0; i < number; i++) {
        if ((i % 3 === 0) || (i % 5 === 0)) {
            sum += i;
        }
    }
    var result = document.getElementById("answer-one");
    result.textContent = sum.toString();
}

function eulerTwo() {
    // Optimization: Every third number in the fibonacci sequence is even. Not implemented here.
    var number = document.getElementById("problem-two").value;
    var result = document.getElementById("answer-two");

    var a = 1;
    var b = 1;
    var c = 0;
    var sum = 0;

    while (b < number) {
        c = a + b;
        if (c % 2 == 0) {
            sum += c;
        }
        a = b + c;
        b = c + a;
    }

    result.textContent = sum.toString();
}

function eulerThree() {
    var number = document.getElementById("problem-three").value;
    var result = document.getElementById("answer-three");
    var factors = [], divideBy = 2;

    while (number > 1) {
        while (number % divideBy === 0) {
            factors.push(divideBy);
            number /= divideBy;
        }

        divideBy += 1;
    }

    factors = Math.max.apply(Math, factors);
    result.textContent = factors.toString();
}


